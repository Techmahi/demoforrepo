<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Regarding_your_first_installment</fullName>
        <description>Regarding your first installment</description>
        <protected>false</protected>
        <recipients>
            <recipient>mounica18695@gmail.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/MarketingProductInquiryResponse</template>
    </alerts>
    <rules>
        <fullName>Time dependant demo</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Payment__c.Instalment_1__c</field>
            <operation>greaterOrEqual</operation>
            <value>500</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Regarding_your_first_installment</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
