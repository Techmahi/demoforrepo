<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Regarding_Adharcard_format</fullName>
        <description>Regarding Adharcard format</description>
        <protected>false</protected>
        <recipients>
            <recipient>mounica18695@gmail.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CustomerPortalChangePwdEmail</template>
    </alerts>
    <alerts>
        <fullName>Regarding_Data_validation_approval_1</fullName>
        <description>Regarding Data validation approval 1</description>
        <protected>false</protected>
        <recipients>
            <recipient>mounica18695@gmail.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SupportCaseAssignmentNotification</template>
    </alerts>
    <alerts>
        <fullName>Regarding_Data_validation_approval_2</fullName>
        <description>Regarding Data validation approval 2</description>
        <protected>false</protected>
        <recipients>
            <recipient>mounica18695@gmail.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SupportEscalatedCaseNotification</template>
    </alerts>
    <alerts>
        <fullName>Regarding_Last_round_results</fullName>
        <description>Regarding Last round results</description>
        <protected>false</protected>
        <recipients>
            <recipient>mounica18695@gmail.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/congratulations</template>
    </alerts>
    <alerts>
        <fullName>Regarding_position_info</fullName>
        <description>Regarding position info</description>
        <protected>false</protected>
        <recipients>
            <recipient>mounica18695@gmail.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Rejection_Mail</template>
    </alerts>
    <alerts>
        <fullName>Regarding_second_round</fullName>
        <description>Regarding second round</description>
        <protected>false</protected>
        <recipients>
            <recipient>mounica18695@gmail.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Regarding_Job_poistion</template>
    </alerts>
    <alerts>
        <fullName>Regarding_your_response_for_coding_round</fullName>
        <description>Regarding your  response for coding round</description>
        <protected>false</protected>
        <recipients>
            <recipient>mounica18695@gmail.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Rejection_Mail</template>
    </alerts>
    <alerts>
        <fullName>Rejection_mail</fullName>
        <description>Rejection mail</description>
        <protected>false</protected>
        <recipients>
            <recipient>mounica18695@gmail.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Rejection_Mail</template>
    </alerts>
    <alerts>
        <fullName>Response_for_your_rejection</fullName>
        <description>Response for your rejection</description>
        <protected>false</protected>
        <recipients>
            <recipient>mounica18695@gmail.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SupportEscalatedCaseNotification</template>
    </alerts>
    <alerts>
        <fullName>Response_for_your_technical_round</fullName>
        <description>Response for your technical round</description>
        <protected>false</protected>
        <recipients>
            <recipient>mounica18695@gmail.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Response_for_your_technical_round</template>
    </alerts>
    <alerts>
        <fullName>Response_for_your_technical_round_rejection</fullName>
        <description>Response for your technical round rejection</description>
        <protected>false</protected>
        <recipients>
            <recipient>mounica18695@gmail.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Rejection_Mail</template>
    </alerts>
    <alerts>
        <fullName>Technical_round</fullName>
        <description>Technical round</description>
        <protected>false</protected>
        <recipients>
            <recipient>mounica18695@gmail.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Regarding_Job_poistion</template>
    </alerts>
    <alerts>
        <fullName>coding_skills</fullName>
        <description>coding skills</description>
        <protected>false</protected>
        <recipients>
            <recipient>mounica18695@gmail.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/congratulations</template>
    </alerts>
    <alerts>
        <fullName>data_validation_mail</fullName>
        <description>data validation mail</description>
        <protected>false</protected>
        <recipients>
            <recipient>mounica18695@gmail.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SupportCaseCreatedPhoneInquiries</template>
    </alerts>
    <alerts>
        <fullName>field_updation</fullName>
        <description>field updation</description>
        <protected>false</protected>
        <recipients>
            <recipient>mounica18695@gmail.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SalesNewCustomerEmail</template>
    </alerts>
    <fieldUpdates>
        <fullName>New_field_update</fullName>
        <field>OwnerId</field>
        <lookupValue>mounica18695@gmail.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>New field update</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Testing demo</fullName>
        <actions>
            <name>Regarding_Adharcard_format</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Demo__c.Adhar_Card__c</field>
            <operation>startsWith</operation>
            <value>A1</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>field updation</fullName>
        <actions>
            <name>New_field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>pls_write_one_program</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Demo__c.Countries__c</field>
            <operation>equals</operation>
            <value>India</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <tasks>
        <fullName>pls_write_one_program</fullName>
        <assignedTo>mounica18695@gmail.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>pls complete with in 3 days</description>
        <dueDateOffset>3</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>pls write  one program</subject>
    </tasks>
</Workflow>
