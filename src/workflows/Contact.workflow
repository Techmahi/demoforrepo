<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Reject_Mail</fullName>
        <description>Reject Mail</description>
        <protected>false</protected>
        <recipients>
            <recipient>batch09@sfdc.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SalesNewCustomerEmail</template>
    </alerts>
    <alerts>
        <fullName>step1_Email</fullName>
        <description>step1 Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>batch09@sfdc.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CommunityChangePasswordEmailTemplate</template>
    </alerts>
</Workflow>
