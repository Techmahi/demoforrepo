trigger CountContactsnew on Contact (after insert, after delete, after undelete) {

    set<id> accIdList = new set<id>();
    if(Trigger.isInsert || Trigger.isUndelete){
        For(Contact con1 : Trigger.new){
            accIdList.add(con1.accountid);
        }
    }
    if(Trigger.isDelete){
        For(Contact con1 : Trigger.old){
            accIdList.add(con1.accountid);
        }
    }
    List<Account> accUpdateList = new List<Account>();
    For(Account acc : [SELECT CountOfContacts__c,(SELECT id FROM Contacts) FROM Account WHERE id =: accIdList])
    {
        acc.CountOfContacts__c = acc.Contacts.size();
        accUpdateList.add(acc);
    }
    
        update accUpdateList;
    
}